#include "includes.h"

.global multiply_instruction

multiply_instruction:

check_width:
	la s1, APU_WIDTH
	lbu t0, 0(s1)
	// Check Single 8-bit Operation
	li t1, WIDTH_8BIT
	li t2, WIDTH_16BIT
	li t3, WIDTH_24BIT
	li t4, WIDTH_32BIT
	li t5, WIDTH_SIMD8
	li t6, WIDTH_SIMD16
	beq t0, t1, multiply_single_8bit
	beq t0, t2, multiply_single_16bit
	beq t0, t3, multiply_single_24bit
	beq t0, t4, multiply_single_32bit
	beq t0, t5, multiply_simd8
	beq t0, t6, multiply_simd16

	// If we're here, bad state, so don't do anything
	ret
	
multiply_single_8bit:
	lhu I, 0(I_ADDR)
	lhu J, 0(J_ADDR)
	mul R, I, J
	sb R, 0(R_ADDR)
	ret

multiply_single_16bit:
	lbu I, 0(I_ADDR)
	lbu J, 0(J_ADDR)
	mul R, I, J
	sb R, 0(R_ADDR)
	ret

multiply_single_24bit:
	lw I, 0(I_ADDR)
	lw J, 0(J_ADDR)
	li a3, MASK_24BIT
	c.and I, a3
	c.and J, a3
	mul R, I, J

	// To preserve registers, grab the 4th value of R
	// store the full 32-bit add, then put the 4th value
	// back.
	lb t0, 4(R_ADDR)
	sw R, 0(R_ADDR)
	sb t0, 4(R_ADDR)
	ret

multiply_single_32bit:
	lw I, 0(I_ADDR)
	lw J, 0(J_ADDR)
	mul R, I, J
	sw R, 0(R_ADDR)
	ret

multiply_simd8:
	lbu I, 0(I_ADDR)
	lbu J, 0(J_ADDR)
	mul R, I, J
	sb I, 0(R_ADDR)

	lbu I, 1(I_ADDR)
	lbu J, 1(J_ADDR)
	mul R, I, J
	sb I, 1(R_ADDR)

	lbu I, 2(I_ADDR)
	lbu J, 2(J_ADDR)
	mul R, I, J
	sb I, 2(R_ADDR)

	lbu I, 3(I_ADDR)
	lbu J, 3(J_ADDR)
	mul R, I, J
	sb I, 3(R_ADDR)
	ret

multiply_simd16:
	lhu I, 0(I_ADDR)
	lhu J, 0(J_ADDR)
	mul R, I, J
	sh I, 0(R_ADDR)

	lhu I, 2(I_ADDR)
	lhu J, 2(J_ADDR)
	mul R, I, J
	sh I, 2(R_ADDR)
	
	ret