#include "includes.h"

.global add_instruction

add_instruction:

check_width:
	la s1, APU_WIDTH
	lbu t0, 0(s1)
	// Check Single 8-bit Operation
	li t1, WIDTH_8BIT
	li t2, WIDTH_16BIT
	li t3, WIDTH_24BIT
	li t4, WIDTH_32BIT
	li t5, WIDTH_SIMD8
	li t6, WIDTH_SIMD16
	beq t0, t1, add_single_8bit
	beq t0, t2, add_single_16bit
	beq t0, t3, add_single_24bit
	beq t0, t4, add_single_32bit
	beq t0, t5, add_simd8
	beq t0, t6, add_simd16
	// If we're here, bad state, so don't do anything
	ret
	
add_single_8bit:
	lbu I, 0(I_ADDR)
	lbu J, 0(J_ADDR)
	c.add I, J
	sb I, 0(R_ADDR)
	ret

add_single_16bit:
	lhu I, 0(I_ADDR)
	lhu J, 0(J_ADDR)
	c.add I, J
	sh R, 0(R_ADDR)
	ret

/*
 Odd one out since we don't have a 3-byte load
*/
add_single_24bit:
	lw I, 0(I_ADDR)
	lw J, 0(J_ADDR)
	li a3, MASK_24BIT
	c.and I, a3
	c.and J, a3
	add R, I, J

	// To preserve registers, grab the 4th value of R
	// store the full 32-bit add, then put the 4th value
	// back.
	lb t0, 4(R_ADDR)
	sw R, 0(R_ADDR)
	sb t0, 4(R_ADDR)
	ret

add_single_32bit:
	lw I, 0(I_ADDR)
	lw J, 0(J_ADDR)
	c.add I, J
	sw I, 0(R_ADDR)
	ret

/* Add each 8-bit pair of I and J into R */
// Unrolled
add_simd8:
	lbu I, 0(I_ADDR)
	lbu J, 0(J_ADDR)
	c.add I, J
	sb I, 0(R_ADDR)

	lbu I, 1(I_ADDR)
	lbu J, 1(J_ADDR)
	c.add I, J
	sb I, 1(R_ADDR)

	lbu I, 2(I_ADDR)
	lbu J, 2(J_ADDR)
	c.add I, J
	sb I, 2(R_ADDR)

	lbu I, 3(I_ADDR)
	lbu J, 3(J_ADDR)
	c.add I, J
	sb I, 3(R_ADDR)
	ret

/* Loop version 
add_simd8:
	mv t0, x0
	li t1, 0x03
add_simd8_loop:	
	add t2, t0, I_ADDR
	lbu I, 0(t2)
	add t2, t0, J_ADDR
	lbu J, 0(t2)
	c.add I, J
	add t2, t0, R_ADDR
	sb I, 0(t2)
	add t0, t0, 1	// Increment t0, our loop index
	bne t0, t1, add_simd8_loop
	j post_instruction_cleanup
*/

/* Add each 16-bit pair of I and J into R */
// Unrolled
add_simd16:
	lhu I, 0(I_ADDR)
	lhu J, 0(J_ADDR)
	c.add I, J
	sh I, 0(R_ADDR)

	lhu I, 2(I_ADDR)
	lhu J, 2(J_ADDR)
	c.add I, J
	sh I, 2(R_ADDR)
	
	ret
