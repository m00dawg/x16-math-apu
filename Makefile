all:
	/opt/riscv/bin/riscv32-unknown-elf-cpp -o apu.opp apu.s
	/opt/riscv/bin/riscv32-unknown-elf-cpp -o add.opp add.s
	/opt/riscv/bin/riscv32-unknown-elf-cpp -o multiply.opp multiply.s
	/opt/riscv/bin/riscv32-unknown-elf-as -march=rv32icm -mabi=ilp32 -o apu.as apu.opp 
	/opt/riscv/bin/riscv32-unknown-elf-as -march=rv32icm -mabi=ilp32 -o add.as add.opp 
	/opt/riscv/bin/riscv32-unknown-elf-as -march=rv32icm -mabi=ilp32 -o multiply.as multiply.opp 
	/opt/riscv/bin/riscv32-unknown-elf-ld -o apu.o *.as

clean:
	rm *.opp *.o *.as