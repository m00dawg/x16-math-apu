#include "includes.h"


.global _start
_start:
main:

setup:
	la INSTRUCTION_ADDR, APU_INSTRUCTION
	la WIDTH_ADDR, APU_WIDTH
	la CONTROL_ADDR, APU_CONTROL
	la STATUS_ADDR, APU_STATUS
	la I_ADDR, APU_I
	la J_ADDR, APU_J
	la R_ADDR, APU_R

// Loop until instruction is not zero
idle_loop:
	lbu a3, 0(INSTRUCTION_ADDR)
	c.beqz a3, idle_loop
lookup_instruction:
	li t1, INST_ADD
	beq a3, t1, add_instruction

post_instruction_cleanup:
	// Set instruction back to 0
	sb x0, 0(s0)
	// Set the operation complete status
	lbu t0, 0(STATUS_ADDR)
	li t1, STATUS_COMPLETE
	or t0, t0, t1
	sb t0, 0(STATUS_ADDR)
	c.j idle_loop

